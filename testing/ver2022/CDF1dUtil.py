#!/usr/bin/env python3

# (C) 2022 Thomas Pitschel
#
# Original code at: https://codeberg.org/thpit/deterministic_bootstrapping
#
# License: see DBT1d.py

__version__ = "2022-05-25"

# 2022-05-24:
# - get_variance()

import numpy as np

class CDF1dUtil:
    
    @staticmethod
    def get_expectation(z_vals, cdf_Z):
        N_ = len(z_vals)
        if len(cdf_Z) != N_: 
            print("Error")
            return None
        buf1 = cdf_Z[1:] - cdf_Z[0:(N_-1)]  # This is the [1:] section of the pdf_z
        res = np.sum(buf1 * z_vals[1:]) + cdf_Z[0] * z_vals[0]
        return res

    @staticmethod
    def get_variance(z_vals, cdf_Z):
        expectation1 = CDF1dUtil.get_expectation(z_vals, cdf_Z)
        if 0:
            return CDF1dUtil.get_expectation(z_vals*z_vals, cdf_Z) - expectation1*expectation1
        else:
            # The below numerically more favourable:
            z_vals_centered = z_vals - expectation1
            return CDF1dUtil.get_expectation(z_vals_centered*z_vals_centered, cdf_Z)
            
        