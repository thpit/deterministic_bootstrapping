#!/usr/bin/env python3

# (C) 2022 Thomas Pitschel
#
# Original code at: https://codeberg.org/thpit/deterministic_bootstrapping
#
# License:
# * Rights not explicitly granted are reserved.
# * Academic use: free to copy, modify and run the code;
#   distribution alongside other software requires permission.
# * Commercial use: free to download and run code for evaluation
#   only. Other use require permission. (contact at above link)
# * All uses: 
#   Modifications must be marked/labelled as such. 
#   Authorship must not be misrepresented.
#   All copies must retain copyright notice and license text.

__version__ = "2022-05-29"

import numpy as np

# 2022-05-28b:
# - adding offset correction in compute_2()
#
# 2022-05-28a:
# - compute_2() and adjust_Y_2()
#   split-adjustment

class DBT1d:
    r"""Devel version

    For given \vec a \in \Real^m, and X^{[i]} \in \Real, i=1...n,
    computes distribution of 
    
        \sum_{j=1}^m a_j * X_j^*,
        
    where X_j^* are bootstrap variables uniformly distributed
    over { X^{[1]}, ..., X^{[n]} }.
    
    ---------------
    
    compute_1() (using adjust_Y_1()): "DBT1d_gl"    (grid-locked)

    compute_2() (using adjust_Y_2()): "DBT1d_gi"    (grid-interpolated)
    
    """

    I = 0+1j
    twopiI = 2 * np.pi * I
    
    @staticmethod
    def adjust_Y_1(y, N, beta_recip, stats):
        """stats is an in-out parameter, and uses pass-by-reference."""
        res = np.round(y)
        buf = (res - y) * beta_recip
        stats[0] += np.abs(buf)
        stats[1] += buf
        return res

    @staticmethod
    def adjust_Y_2(y, N, beta_recip, stats):
        """stats is an in-out parameter, and uses pass-by-reference."""
        resA = np.floor(y)
        resB = np.ceil(y)
        resA <= y <= resB
        if resA < resB:
            # Ensure wA * resA + wB * resB = y
            wA = (resB-y) / (resB-resA)
            wB = 1-wA   # = (y-resA)/(resB-resA)
        else:
            wA = wB = 0.5
        buf = (wA * (y - resA) + wB * (resB - y)) * beta_recip
        stats[0] += np.abs(buf)
        stats[1] += wA * resA + wB * resB - y
        return resA,resB,wA,wB

    @staticmethod
    def compute(a, X, N, do_debug=False):
        # To return a 2-tupel (z_vals, f_z)
        return DBT1d.compute_2(a, X, N, do_debug=do_debug)

    @staticmethod
    def compute_2(a, X, N, do_debug=False):
        # Make sure beta in X[i] * beta is such that 
        # the support of \sum_j beta * a_j * X_j^* has length <= N/2.
        
        m = len(a)
        n = len(X)
        
        minX = np.min(X)
        X = X - minX
        
        Mx = np.max(X)
        Mn = np.min(X)
        
        a_pos = a * (1*(a > 0))
        a_neg = a * (1*(a < 0))
        a_plus  = np.sum(a_pos)
        a_minus = np.sum(a_neg)
        
        h = max(Mx * a_plus, Mn * a_minus) - min(Mn * a_plus, Mx * a_minus)
            # h = supp \sum_j a_j * X_j^*
        
        beta = N / (2*h)            
        beta_recip = 1.0 / beta
        #if h < N//4:
        #    beta = N // (2*h)
        
        stats = np.zeros((2,))
        
        # Y = a_j X_1^*
        FT_f_Y = 0*DBT1d.I + np.zeros((m,N))  # ensure imaginary capacity
        for j in range(m):
            buf2 = 0*DBT1d.I + np.zeros((N,))
            for i in range(n):
                # For each sample, have now two rows of factors.
                Y_adjustedA,Y_adjustedB,wA,wB = DBT1d.adjust_Y_2(a[j] * X[i] * beta, N, beta_recip, stats)
                facA = np.exp(-DBT1d.twopiI * Y_adjustedA / N)
                facB = np.exp(-DBT1d.twopiI * Y_adjustedB / N)
                #if j==0: print(X[i])
                buf1A = 0*DBT1d.I + np.ones((N,))
                buf1B = 0*DBT1d.I + np.ones((N,))
                for k in range(1, N):
                    buf1A[k] = buf1A[k-1] * facA
                    buf1B[k] = buf1B[k-1] * facB
                buf2 += (wA*buf1A + wB*buf1B) / n
            FT_f_Y[j,:] = buf2

        #print(FT_f_Y)
        #IFT = np.fft.ifft(FT_f_Y[0,:])
        #print(IFT)

        FT_f_Z = np.prod(FT_f_Y[:,:], axis=0)
        #print(FT_f_Z.shape)
        IFT = np.fft.ifft(FT_f_Z)
        
        if do_debug:
            print(np.sum(IFT))
            print("Total abs error:", stats[0])
            print("Mean shift:     ", stats[1])
        
        z_vals = np.arange(0, N) / beta
        #IFT -= IFT[0]
        #IFT = IFT / np.sum(IFT)
        
        return z_vals + minX * np.sum(a), np.real(IFT)
        
    @staticmethod
    def compute_1(a, X, N, do_debug=False):
        # Make sure beta in X[i] * beta is such that 
        # the support of \sum_j beta * a_j * X_j^* has length <= N/2.
        
        m = len(a)
        n = len(X)
        
        Mx = np.max(X)
        Mn = np.min(X)
        
        a_pos = a * (1*(a > 0))
        a_neg = a * (1*(a < 0))
        a_plus  = np.sum(a_pos)
        a_minus = np.sum(a_neg)
        
        h = max(Mx * a_plus, Mn * a_minus) - min(Mn * a_plus, Mx * a_minus)
            # h = supp \sum_j a_j * X_j^*
        
        beta = N / (2*h)            
        beta_recip = 1.0 / beta
        #if h < N//4:
        #    beta = N // (2*h)
        
        stats = np.zeros((2,))
        
        # Y = a_j X_1^*
        FT_f_Y = 0*DBT1d.I + np.zeros((m,N))  # ensure imaginary capacity
        for j in range(m):
            buf2 = 0*DBT1d.I + np.zeros((N,))
            for i in range(n):
                fac = np.exp(-DBT1d.twopiI * DBT1d.adjust_Y_1(a[j] * X[i] * beta, N, beta_recip, stats) / N)
                #if j==0: print(X[i])
                buf1 = 0*DBT1d.I + np.ones((N,))
                for k in range(1, N):
                    buf1[k] = buf1[k-1] * fac
                buf2 += buf1 / n
            FT_f_Y[j,:] = buf2

        #print(FT_f_Y)
        #IFT = np.fft.ifft(FT_f_Y[0,:])
        #print(IFT)

        FT_f_Z = np.prod(FT_f_Y[:,:], axis=0)
        #print(FT_f_Z.shape)
        IFT = np.fft.ifft(FT_f_Z)
        
        if do_debug:
            print(np.sum(IFT))
            print("Total abs error:", stats[0])
            print("Mean shift:     ", stats[1])
        
        z_vals = np.arange(0, N) / beta
        #IFT -= IFT[0]
        #IFT = IFT / np.sum(IFT)
        
        return z_vals, np.real(IFT)

    @staticmethod
    def get_cdf(z_vals, pdf_z):
        return np.cumsum(pdf_z)

    @staticmethod
    def get_expectation(z_vals, cdf_Z):
        N_ = len(z_vals)
        buf1 = cdf_Z[1:] - cdf_Z[0:(N_-1)]  # This is the [1:] section of the pdf_z
        res = np.sum(buf1 * z_vals[1:]) + cdf_Z[0] * z_vals[0]
        return res

if __name__ == "__main__":
    
    import numpy.random as rnd

    n = 10
    X = np.arange(0,n, dtype=np.float32)
    X = X + rnd.uniform(size=(n,))*0.1
    
    X = X+113
    
    m = 5
    a = np.ones((m,))
    #a[2] = -1

    print("Expectation (in): ", np.sum(a) * np.mean(X))
    
    N = 100
    
    z_vals, pdf_z = DBT1d.compute(a, X, N, do_debug=True)
    #print(pdf_z[0:10])
    #print(pdf_z[-20:-1])
    
    import matplotlib.pyplot as plt
    plt.plot(z_vals, pdf_z, label="Bootstrap density")
    plt.legend()
    plt.show()
    
    cdf_Z = DBT1d.get_cdf(z_vals, pdf_z)
    plt.plot(z_vals, cdf_Z, label="Bootstrap cdf")
    plt.legend()
    plt.show()
    
    print("Expectation (raw):", DBT1d.get_expectation(z_vals, cdf_Z))
    