#Darwin, C. (1876) The Effect of Cross and Self-Fertilization in the Vegetable Kingdom, 2nd edition, London.
# final heights of each of 2 x 15 plants after a fixed (common?) period of time.
# Values in inches as seen in "The Design of Experiments", R.A.Fisher, 1935, pg 30.
# pair no,  cross-fertilized, self-fertilized
1,23.5,17.375
2,12.0,20.375
3,21.0,20.0
4,22.0,20.0
5,19.125,18.375
6,21.5,18.625
7,22.125,18.625
8,20.375,15.25
9,18.25,16.5
10,21.625,18.0
11,23.25,16.25
12,21.0,18.0
13,22.125,12.75
14,23.0,15.5
15,12.0,18.0
