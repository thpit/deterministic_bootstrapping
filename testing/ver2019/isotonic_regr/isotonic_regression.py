# python3
# -*- coding: utf-8 -*-

# (C) 2019 Thomas Pitschel
#
# Original code at: https://codeberg.org/thpit/deterministic_bootstrapping
#
# License:
# * Rights not explicitly granted are reserved.
# * Academic use: free to copy, modify and run the code;
#   distribution alongside other software requires permission.
# * Commercial use: free to download and run code for evaluation
#   only. Other use require permission. (contact at above link)
# * All uses: 
#   Modifications must be marked/labelled as such. 
#   Authorship must not be misrepresented.
#   All copies must retain copyright notice and license text.

#
# Changelog:
#
#  2019-09-24  
#    
# Note: Remove "###" for debug output.

import numpy as np

class IsotonicRegression:
    '''Computes approximation to one-dimensional real-valued input sequence, 
        yielding a non-decreasing sequence. Currently only implemented:
        minimization of ell_2 norm (below called "L2").'''

    def __init__(self):
        pass

    def compute_by_L2_penalty__countlist(self, x):
        '''Computes the approximation to x (expected as 1D array of
            real values) such that the result is non-decreasing
            and minimizes the sum-of-squares error to the input
            sequence.
            Returns a count list, array of elems of format 
            [val,count].'''

        # Implementation can be made efficient by use of the concept
        # of averaging.
     
        N = len(x); # expect array

        # proceeds by maintaining a list of past traversed values.
        # Upon encountering a monotonicity "miss", adjusts the currently 
        # seen and past placed data points such that the quadratic 
        # error is minimized.

        # buf will be nSeenPoints x 2. (first component denotes value, second the count)
        buf = [[x[0], 1]];
        for i in range(1,N):
            ###print('Current buf:', buf);
            cand = x[i];
            cand_count = 1;
            #ii2 = len(buf)-1;
            #count2 = 0;
            #avg2 = 0.0;
            doloop = 0;
            while (len(buf) > 0):
                latest_elem_v_c = buf.pop();
                if latest_elem_v_c[0] < cand:
                    ###print("<");
                    buf.append(latest_elem_v_c);
                    buf.append([cand, cand_count]);
                    doloop = 1;
                    break;
                if latest_elem_v_c[0] <= cand:
                    ###print("<=");
                    buf.append([cand, latest_elem_v_c[1]+cand_count]);
                    doloop = 1;
                    break;
                # Otherwise the sequence is decreasing
                ###print(">");
                avg2 = 0.0 + latest_elem_v_c[0] * latest_elem_v_c[1] + cand * cand_count;
                count2 = latest_elem_v_c[1] + cand_count;
                cand = avg2/count2;
                cand_count = count2;
            if doloop > 0: continue;
            # At this point, have absorbed all past values from
            # the left side that were larger than the current 
            # candidate. Append the result:
            buf.append([cand, cand_count]);
            ###print("Having appended {0},{1}.".format(cand,cand_count));
        # Finally we have in buf the result values together with their
        # count. 
        ###print('Current buf:', buf);
        ###print(np.sum(np.matrix(buf)[:,1])); # checks whether sum of counts is equal to input length
        return buf;

    def countlist_to_plain(self, N, buf):
        '''Expands the compressed representation into an array
            of size N.'''
            
        res = np.zeros((N));
        i3 = N-1;
        while len(buf) > 0:
            latest_elem_v_c = buf.pop();
            c = latest_elem_v_c[1]-1;
            while c >= 0:
                res[i3] = latest_elem_v_c[0];
                i3 -= 1;
                c -= 1;
        return res;

    def countlist_to_sparse(self, clist, vals_plain):
        '''Given the countlist in "clist" and effective values
            associated to index positions in "vals_plain", returns
            two arrays f_sp,vals_sp of equal length (<= 2*L), where
            L is the length of "clist", such that 
              f_sp   [    2*k] = clist[k,0]   and
              f_sp   [1 + 2*k] = clist[k,0],
              vals_sp[    2*k] = vals_plain[     \sum_{j=0}^{k-1} clist[j,1]]   and
              vals_sp[1 + 2*k] = vals_plain[-1 + \sum_{j=0}^{k}   clist[j,1]].
           Thus f_sp and vals_sp contain the support values and places
           with which to construct the function meant with clist.
        '''
        # Note that in a sense, clist is L x 2, while the result
        # constructed is 2 x (2*L).

        L = len(clist);
        f_sp = [];
        vals_sp = [];
        ind_sum = 0;
        k = 0;
        for elem_pair in clist:
            #f_sp[2*k  ] = elem_pair[0];
            f_sp.append(elem_pair[0]);
            #vals_sp[2*k  ] = vals_plain[ind_sum];
            vals_sp.append(vals_plain[ind_sum]);
            ind_sum += elem_pair[1];
            if elem_pair[1] > 1: # (avoids duplicate)
                #f_sp[2*k+1] = elem_pair[0];
                f_sp.append(elem_pair[0]);
                #vals_sp[2*k+1] = vals_plain[ind_sum-1];
                vals_sp.append(vals_plain[ind_sum-1]);
            k += 1;
        return f_sp,vals_sp;

    def countlist_to_sparse_density(self, start_y, clist, places_plain):
        '''As before, but here returning only the increments of
            the function f implied by clist.
            This function chooses to associate the jump to the
            midvalue of the interval boundaries.
            (Note: Output is lossy in the sense that it does not
            allow to reconstruct the function f, unless places_plain
            is used.)
            start_y is the y-value at which the first input value
            is compared against. (usually 0.0)
        '''
        # The given vals_plain are deemed place values at the _boundaries_
        # of the sample intervals. A jump therefore is associated to 
        # the inner part of the interval.
        L = len(clist);
        f_dens_sp = [];
        vals_sp = []; # output places, sparse
        ind_sum = 0;
        k = 0;
        prev = start_y;
        eff_place = 0.0;
        for elem_pair in clist:
            f_dens_sp.append(elem_pair[0]-prev);
            vals_sp.append(eff_place);
            ind_sum += elem_pair[1];
            if ind_sum < len(places_plain):
                eff_place = (places_plain[ind_sum-1] + places_plain[ind_sum])/2; # mid value
            prev = elem_pair[0];
        return f_dens_sp,vals_sp;

    def compute_by_L2_penalty(self, x):
        '''As above compute_by_L2_penalty__countline, but here returns 
            array of size as the input.'''
        N = len(x);
        buf = self.compute_by_L2_penalty__countlist(x);
        return self.countlist_to_plain(N, buf);


