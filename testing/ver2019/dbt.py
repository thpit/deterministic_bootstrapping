# python3
# -*- coding: utf-8 -*-

# (C) 2019 Thomas Pitschel
#
# Original code at: https://codeberg.org/thpit/deterministic_bootstrapping
#
# License:
# * Rights not explicitly granted are reserved.
# * Academic use: free to copy, modify and run the code;
#   distribution alongside other software requires permission.
# * Commercial use: free to download and run code for evaluation
#   only. Other use require permission. (contact at above link)
# * All uses: 
#   Modifications must be marked/labelled as such. 
#   Authorship must not be misrepresented.
#   All copies must retain copyright notice and license text.


# Changelog:
#
# 2019-09-27    Adding dbt_get_cdf_monotonous().
#               Adding dbt_get_lower_quantile()
#               Adding dbt_get_upper_quantile()
#
# 2019-07-09c   dbt_get_cdf()
#               dbt_density_twosample() [untested]
#
# 2019-07-09b   dbt_get_variance_div_N()
#               This code is now calibrated. (This employs the +0.5 T/N
#               correction, which is necessary when using weight_mode == 21.)
#
# 2019-07-09    offset correction for fractional part of cyclical shift.
#
# 2019-07-08    - correction to have exp(-(...)) instead of exp((...))
#                 in the forward trafo part.
#               - adding tilde_f shift.
#
# 2019-04-28    start

import numpy as np
import random as rn
from isotonic_regr.isotonic_regression import IsotonicRegression

twopij = 2 * np.pi * 1j;

class dbt:
    '''Methods for computing density from discretely distr. X.'''
    
    def __init__(self):
        self.n = 0;
        self.m = 0;
        
    def pospart(self, x):
        if x > 0.0:
            return x;
        return 0.0;

    def negpart(self, x):
        if x < 0.0:
            return x;
        return 0.0;
            
    # Some comments on the code:
    # - np.tensordot(a_prime, X, 0) is a tensor product with contraction in zero modes, i.e. no contraction;
    #   i.e. this yields Kronecker prod. (This is independent of orientation of the vectors a_prime and X.)
    # - FT_aX = FT_aX + 0j ensures data type is complex. Needed later during setting in single columns.
    # - np.multiply(Q_curr, Q_step); is element-wise multiplic.
       
    def dbt_density(self, X, a, N, Tfac = 1.05):
        '''Computes the pdf of \sum_j a_j X^j where X^j are distributed as \hat F_n
            derived from the sample X. It is N the algorithm resolution. 
            Returns a 2 x N array-like containing in the first row the computed 
            f_Z and in the second row the z-values corresponding to 
            the respective index position (z_vals).'''
            
        n = self.n = len(X);
        m = self.m = len(a);
        ###print('N:               {0}'.format(N));
        ###print('n:               {0}'.format(n));
        ###print('m:               {0}'.format(m));
        
        #print(np.abs(a))
        a_pos = np.sum([self.pospart(x) for x in a]);
        a_neg = np.sum([self.negpart(x) for x in a]);
        z_max = a_pos * max(X) + a_neg * min(X);
        z_min = a_pos * min(X) + a_neg * max(X);
        T_Z = z_max - z_min;
        #Tfac = 1.05;
        ###print('Tfac:            '.format(Tfac));
        T = T_Z * Tfac;
        #print 'a_pos', a_pos
        #print 'a_neg', a_neg
        ###print('z_min, z_max:    {0},{1}'.format(z_min, z_max));
        ###print('T_Z:             {0}'.format(T_Z));
        ###print('T:               {0}'.format(T));
        u = 1/T;
        a_prime = np.multiply(a, -twopij * u);
        Q_step = np.exp(np.tensordot(a_prime, X, 0));       # 2 \pi i a X^T
        Q_curr = np.ones((m,n)); #OLD: np.tensordot(np.ones(m), np.ones(n), 0);   # = ones(m, n)
        FT_aX = np.zeros((m,N)); #OLD: np.tensordot(np.zeros(m), np.zeros(N), 0);  # = zeros(m, N)
        FT_aX = FT_aX + 0j; # ensure data type is complex
        n_recip = 1.0/n;
        for k in range(0,N):  # from 0 to N-1
            #print(Q_curr)
            FT_aX[:,k] = n_recip*np.sum(Q_curr, 1); # assign the sum (along rows) to the k-th column in G  (zero-based)
            Q_curr = np.multiply(Q_curr, Q_step);   # element-wise product
        ###print(FT_aX);    # an (m x N) matrix
        FT_Z = np.prod(FT_aX, 0);
        ###print(FT_Z);     # an N-dim row vector
        weight_mode = 21;
        if weight_mode == 21:
            kbuf = (0.0 + np.array(range(1,N)))/N; # = [1.0/N, 2.0/N, ..., (N-1)/N]
            kbuftpj = twopij * kbuf;
            weights_pre = (np.exp(kbuftpj) - 1)/kbuftpj;
            weights = np.concatenate([[1], weights_pre]);
            #print weights
            FT_Z = FT_Z * weights; # Hadamard-like multiplication
        tilde_h = np.fft.ifft(FT_Z);
        tilde_f = 2 * np.real(tilde_h) - 1.0/N;
        ###print tilde_f
        
        # Shift f_Z to place the logical zero index position
        # to the correct actual position in the array.
        buf1 = -z_min/T_Z*N;
        shift1 = int(round(buf1)); 
        ###print('shift:      {0}'.format(shift1));
        if shift1 > 0:
            # If shift1 > 0, put shift1 values of the right part of array tilde_f
            # to the left in f_Z:
            f_Z = np.concatenate([tilde_f[slice(N-shift1, N)], tilde_f[slice(0, N-shift1)]])
            # [slice(a,b)] returns values at indices a, ..., b-1  (which are zerobased)
        else:
            # otherwise put shift1 elements from the left to the right:
            if shift1 != 0:
                f_Z = np.concatenate([tilde_f[slice(-shift1, N)], tilde_f[slice(0, -shift1)]])
            else:
                f_Z = tilde_f
        # If buf1-shift1 is positive, the shift of the z_vals is effectively larger
        # than what is needed. The expectation would thus result to be lower. To correct,
        # add the offset to the z_vals below:
        offs_corr = (buf1-shift1)*T/N;
        ###print('offs_corr: {0}'.format(offs_corr));
        z_vals = offs_corr + (z_min/T_Z + (0.5+np.array(range(0,N)))/N) * T; ##003
        return [f_Z, z_vals];
        
    def dbt_density_twosample(self, X1, X2, a1, a2, N, Tfac = 1.05):
        '''Computes the pdf of the rv Z = \sum_j a_j X^{[j]} + \sum_j a_j' X^{[j]}',
            where the X^{[j]} are iid(\sim X1) and X^{[j]}' are iid(\sim X_2).'''

        n1 = len(X1);
        n2 = len(X2);
        m1 = len(a1);
        m2 = len(a2);
        print('N:               '.format(N));
        print('n1:              '.format(n1));
        print('n2:              '.format(n2));
        print('m1:              '.format(m1));
        print('m1:              '.format(m2));
        
        #print(np.abs(a))
        #a1_pos = reduce(self.add_if_positive, a1, 0);
        #a1_neg = reduce(self.add_if_negative, a1, 0);
        #a2_pos = reduce(self.add_if_positive, a2, 0);
        #a2_neg = reduce(self.add_if_negative, a2, 0);
        a1_pos = np.sum([self.pospart(x) for x in a1]);
        a1_neg = np.sum([self.negpart(x) for x in a1]);
        a2_pos = np.sum([self.pospart(x) for x in a2]);
        a2_neg = np.sum([self.negpart(x) for x in a2]);
        z1_max = a1_pos * max(X1) + a1_neg * min(X1);
        z1_min = a1_pos * min(X1) + a1_neg * max(X1);
        z2_max = a2_pos * max(X2) + a2_neg * min(X2);
        z2_min = a2_pos * min(X2) + a2_neg * max(X2);
        z_max = z1_max + z2_max;
        z_min = z1_min + z2_min;
        T_Z = z_max - z_min;
        #Tfac = 1.05;
        print('Tfac:            '.format(Tfac));
        T = T_Z * Tfac;
        print('z_min, z_max:    '.format(z_min, z_max));
        print('T_Z:             '.format(T_Z));
        print('T:               '.format(T));
        u = 1/T;
        a1_prime = np.multiply(a1, -twopij * u);
        a2_prime = np.multiply(a2, -twopij * u);

        Q_step = np.exp(np.tensordot(a1_prime, X1, 0));       # 2 \pi i a X^T
        Q_curr = np.ones((m1,n1)); #np.tensordot(np.ones(m1), np.ones(n1), 0);   # = ones(m1, n1)
        FT_a1X1 = np.zeros((m1,N)); #np.tensordot(np.zeros(m1), np.zeros(N), 0); # = zeros(m1, N)
        FT_a1X1 = FT_a1X1 + 0j; # ensure data type is complex
        n_recip = 1.0/n1;
        for k in range(0,N):  # from 0 to N-1
            FT_a1X1[:,k] = n_recip * np.sum(Q_curr, 1); # assign the sum (along rows) to the k-th column in G  (zero-based)
            Q_curr = np.multiply(Q_curr, Q_step);       # element-wise product

        Q_step = np.exp(np.tensordot(a2_prime, X2, 0));       # 2 \pi i a' X'^T
        Q_curr = np.ones((m2,n2)); #np.tensordot(np.ones(m2), np.ones(n2), 0);   # = ones(m2, n2)
        FT_a2X2 = np.zeros((m2,N)); #np.tensordot(np.zeros(m2), np.zeros(N), 0); # = zeros(m2, N)
        FT_a2X2 = FT_a2X2 + 0j; # ensure data type is complex
        n_recip = 1.0/n2;
        for k in range(0,N):  # from 0 to N-1
            FT_a2X2[:,k] = n_recip * np.sum(Q_curr, 1); # assign the sum (along rows) to the k-th column in G  (zero-based)
            Q_curr = np.multiply(Q_curr, Q_step);       # element-wise product

        FT_Z = np.prod(FT_a1X1, 0) * np.prod(FT_a2X2, 0); # Hadamard-like
        
        # The below is duplicated from dbt_density():
        weight_mode = 21;
        if weight_mode == 21:
            kbuf = (0.0 + np.array(range(1,N)))/N; # = [1.0/N, 2.0/N, ..., (N-1)/N]
            kbuftpj = twopij * kbuf;
            weights_pre = (np.exp(kbuftpj) - 1)/kbuftpj;
            weights = np.concatenate([[1], weights_pre]);
            #print weights
            FT_Z = FT_Z * weights; # Hadamard-like multiplication
        tilde_h = np.fft.ifft(FT_Z);
        tilde_f = 2 * np.real(tilde_h) - 1.0/N;
        #print tilde_f
        
        # Shift f_Z to place the logical zero index position
        # to the correct actual position in the array.
        buf1 = -z_min/T_Z*N;
        shift1 = int(round(buf1)); 
        ###print('shift:      '.format(shift1));
        if shift1 > 0:
            # If shift1 > 0, put shift1 values of the right part of array tilde_f
            # to the left in f_Z:
            f_Z = np.concatenate([tilde_f[slice(N-shift1, N)], tilde_f[slice(0, N-shift1)]])
            # [slice(a,b)] returns values at indices a, ..., b-1  (which are zerobased)
        else:
            # otherwise put shift1 elements from the left to the right:
            if shift1 != 0:
                f_Z = np.concatenate([tilde_f[slice(-shift1, N)], tilde_f[slice(0, -shift1)]])
            else:
                f_Z = tilde_f
        # If buf1-shift1 is positive, the shift of the z_vals is effectively larger
        # than what is needed. The expectation would thus result to be lower. To correct,
        # add the offset to the z_vals below:
        offs_corr = (buf1-shift1)*T/N;
        ###print('offs_corr: '.format(offs_corr));
        z_vals = offs_corr + (z_min/T_Z + (0.5+np.array(range(0,N)))/N) * T; ##003
        return [f_Z, z_vals];
        
    def dbt_get_expectation(self, f_Z, z_vals):
        '''Expects in f_Z the normalized density as computed by
            dbt_density(). Returns the expectation of the pdf.'''
        return sum(f_Z * z_vals);
            
    def dbt_get_variance_div_N(self, f_Z, z_vals):
        '''Expects in f_Z the normalized density as computed by
            dbt_density(). Returns the variance of the pdf.'''
        mean1 = self.dbt_get_expectation(f_Z, z_vals);
        buf = z_vals - mean1;
        buf = buf * buf; 
        return sum(f_Z * buf);
            
    def dbt_get_cdf(self, f_Z):
        '''Expects in f_Z the normalized density as computed by
            dbt_density(). Returns \tilde F_i = \sum_{k=0}^{i-1} f_Z[k],
            which can be regarded as the left-edge value of the cdf
            for the bin i.'''
        ls1 = [0.0];
        buf = 0.0;
        for i in range(0,len(f_Z)-1): 
            buf = buf + f_Z[i];
            ls1.append(buf);
        return np.array(ls1);
        
    def dbt_get_cdf_monotonous(self, f_Z):
        F_Z = self.dbt_get_cdf(f_Z);
        irgr1 = IsotonicRegression();
        F_Z_ndecreas = irgr1.compute_by_L2_penalty(F_Z); # plain (i.e. non-sparse) 
        return F_Z_ndecreas;

    def dbt_get_lower_quantile(self, F_Z, z_vals, alpha):  
        '''Note this takes the cdf F_Z as input. Returns the
            largest z value with F_Z(z) <= \alpha. Expects
            alpha > 0.'''
        N = len(F_Z);
        if F_Z[0] > alpha:
            return 2*z_vals[0] - z_vals[1];
        for i in range(1,N):
            if F_Z[i] > alpha:
                return z_vals[i-1];
        return z_vals[N-1];

    def dbt_get_upper_quantile(self, F_Z, z_vals, alpha):  
        '''Note this takes the cdf F_Z as input. Returns the
            smallest z value with F_Z(z) >= 1-\alpha. Expects
            alpha > 0.'''
        N = len(F_Z);
        calpha = 1 - alpha;
        if F_Z[N-1] < calpha:
            return 2*z_vals[N-1] - z_vals[N-2];
        for i in range(1,N):
            if F_Z[N-1-i] < calpha:
                return z_vals[N-i];
        return z_vals[0];
            
    def test_example(self, noise_type, coeff_type, N):
        n = self.n = 50;
        m = self.m = 5;
        if (noise_type == 1):
            print('X ~ unif[-.5,.5]:');
            X = np.random.rand(n);
            X = X - np.mean(X);
            print(X);
        if (coeff_type == 5):
            a = [1,1,1,1,1];
        return self.dbt_density(X, a, N);

    

if 0:
    xx = [1.0, 1.2, 0.8, 0.9]
    a = [1,1,1,1,1];
    
    #print(xx);
    yy = np.fft.fft(xx);
    
    print(np.fft.ifft(yy));
    
    dd1 = dbt_density();
    X = [1,1,2,1,1,-1];
    dd1.compute_density(X, [1,-1,1.3,-1.1], 100);
    
    
    print();
    print('Uniformly random input:');
    dd1.test_dbt(1, 5, 100);



