# Deterministic bootstrapping library

This repo contains python code for deterministic bootstrapping. 
Currently this is in advanced test stage, yet not yet packaged.

![Applied to Darwin plant data](testing/imgs/notebook_screenshot_darwin2.png)

## License and Citation

testing/ directory:

Currently all files carry the same license, which is geared towards academic use,
see testing/DBT1d.py. Distribution of the code currently is restricted.

Please cite any academic use of the software, if published, using

* <i>Efficient computation of the cumulative distribution function of a linear mixture of independent random variables</i>, Th.P., 2019, doi: 10.48550/arXiv.1906.07186


## Contact

dl80qaba@studserv.uni-leipzig.de




